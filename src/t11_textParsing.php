<?php

$data = '<synonym words="Billion| Million">Trillion</synonym> is a <synonym words="extremely| |generously">very</synonym> <synonym words="tiny| little| smallish| short">small</synonym> stage in a vast <synonym words="galactic| | large| huge| tense| big">cosmic</synonym> <synonym words="universe| Colosseum| planet">arena</synonym>.';

echo strip_tags($data);

/**
 * {Billion|Million|Trillion} is a {extremely||generously|very} {tiny|little|smallish|short|small} stage in a vast {galactic| |large|huge|tense|big|cosmic} {universe|Colosseum|planet|arena}.
 * 
 **/

$data = str_replace("</synonym>","}",$data);
$data = str_replace("\">","|",$data);
$data = str_replace("<synonym words=\"","{",$data);


echo $data;