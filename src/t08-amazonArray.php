<?php

$products = array(
    'us-B004X6ABTM' => array(
        'price' => '44.96',
        'priceOld' => '59.22',
        'percentageSaved' => 24,
        'currency' => '$',
        'currencyCode' => 'USD',
        'manufacturer' => 'MSI COMPUTER',
        'category' => '',
        'categoryPath' => array(),
        'merchant' => 'Amazon.com',
        'logo' => '',
        'domain' => 'amazon.com',
        'rating' => '',
        'reviewsCount' => '',
        'availability' => '',
        'orig_url' => '',
        'ean' => '8169090853896',
        'upc' => '',
        'sku' => '',
        'isbn' => '',
        'woo_sync' => '',
        'woo_attr' => '',
        'features' => array(
            0 => array(
                'name' => 'Aspect Ratio',
                'value' => 'Unknown'
            ),
            1 => array(
                'name' => 'Binding',
                'value' => 'Personal Computers'
            ),
            2 => array(
                'name' => 'Brand',
                'value' => 'MSI'
            ),
            3 => array(
                'name' => 'Catalog Number List',
                'value' => 'Catalog Number List Element: KD4522'
            ),
            4 => array(
                'name' => 'EAN',
                'value' => '8169090853896'
            ),
            5 => array(
                'name' => 'EAN List',
                'value' => '8169090853896; 0731215283277; 0115970740051; 0172304350980; 7887117165975; 0803982958068; 0763615963824; 0807320189633; 0367679947624; 0816909085389; 0071030051492; 0782386485793; 7337331850350; 0320127567744; 5554442247165; 0168141640445; 5053460940091; 0088020594266; 0777786449247; 7123290460606; 4526541091544; 0132018560907; 0658462265934; 7426100203241; 0088038032392; 5596692266862'
            ),
            6 => array(
                'name' => 'Item Dimensions',
                'value' => 'Height: 71; Length: 571; Weight: 65; Width: 272'
            ),
            7 => array(
                'name' => 'Label',
                'value' => 'MSI COMPUTER'
            ),
            8 => array(
                'name' => 'Languages',
                'value' => 'Name: English; Type: Unknown'
            ),
            9 => array(
                'name' => 'Legal Disclaimer',
                'value' => 'BRAND NEW NEVER OPENED IN THE ORIGINAL BOX! Shipping on the same or next business day with checking number!'
            ),
            10 => array(
                'name' => 'Manufacturer',
                'value' => 'MSI COMPUTER'
            ),
            11 => array(
                'name' => 'Model',
                'value' => 'R6450-MD1GD3/LP'
            ),
            12 => array(
                'name' => 'MPN',
                'value' => 'R6450-MD1GD3/LP'
            ),
            13 => array(
                'name' => 'Number Of Items',
                'value' => '1'
            ),
            14 => array(
                'name' => 'Package Dimensions',
                'value' => 'Height: 173; Length: 902; Weight: 80; Width: 626'
            ),
            15 => array(
                'name' => 'Package Quantity',
                'value' => '1'
            ),
            16 => array(
                'name' => 'Part Number',
                'value' => 'R6450-MD1GD3/LP'
            ),
            17 => array(
                'name' => 'Product Group',
                'value' => 'CE'
            ),
            18 => array(
                'name' => 'Product Type Name',
                'value' => 'COMPUTER_COMPONENT'
            ),
            19 => array(
                'name' => 'Publisher',
                'value' => 'MSI COMPUTER'
            ),
            20 => array(
                'name' => 'Size',
                'value' => '1GB'
            ),
            21 => array(
                'name' => 'Studio',
                'value' => 'MSI COMPUTER'
            ),
            22 => array(
                'name' => 'Title',
                'value' => 'MSI ATI Radeon HD6450 1 GB DDR3 VGA/DVI/HDMI Low Profile PCI-Express Video Card R6450-MD1GD3/LP'
            ),
            23 => array(
                'name' => 'UPC',
                'value' => '071030051492'
            ),
            24 => array(
                'name' => 'UPC List',
                'value' => '071030051492; 658462265934; 367679947624; 132018560907; 168141640445; 807320189633; 088020594266; 777786449247; 115970740051; 763615963824; 172304350980; 782386485793; 731215283277; 816909085389; 320127567744; 803982958068; 088038032392'
            )
        ),
        'unique_id' => 'us-B004X6ABTM',
        'title' => 'MSI ATI Radeon HD6450 1 GB DDR3 VGA/DVI/HDMI Low Profile PCI-Express Video Card R6450-MD1GD3/LP',
        'description' => '',
        'img' => 'https://demo-wordpress-minrig-pisco.c9users.io/wp-content/uploads/2018/04/msi-ati-radeon-hd6450-1-gb-ddr3-vgadvihdmi-low-profile-pci-express-video.jpg',
        'url' => 'https://www.amazon.com/MSI-Profile-PCI-Express-R6450-MD1GD3-LP/dp/B004X6ABTM?psc=1&SubscriptionId=AKIAIZ3S74HU3GOABUYQ&tag=food-processr-20&linkCode=xm2&camp=2025&creative=165953&creativeASIN=B004X6ABTM',
        'last_update' => 1523215962,
        'extra' => array(
            'locale' => 'us',
            'associate_tag' => 'food-processr-20',
            'itemLinks' => array(
                0 => array(
                    'Description' => 'Technical Details',
                    'URL' => 'https://www.amazon.com/MSI-Profile-PCI-Express-R6450-MD1GD3-LP/dp/tech-data/B004X6ABTM?SubscriptionId=AKIAIZ3S74HU3GOABUYQ&tag=food-processr-20&linkCode=xm2&camp=2025&creative=386001&creativeASIN=B004X6ABTM'
                ),
                1 => array(
                    'Description' => 'Add To Baby Registry',
                    'URL' => 'https://www.amazon.com/gp/registry/baby/add-item.html?asin.0=B004X6ABTM&SubscriptionId=AKIAIZ3S74HU3GOABUYQ&tag=food-processr-20&linkCode=xm2&camp=2025&creative=386001&creativeASIN=B004X6ABTM'
                ),
                2 => array(
                    'Description' => 'Add To Wedding Registry',
                    'URL' => 'https://www.amazon.com/gp/registry/wedding/add-item.html?asin.0=B004X6ABTM&SubscriptionId=AKIAIZ3S74HU3GOABUYQ&tag=food-processr-20&linkCode=xm2&camp=2025&creative=386001&creativeASIN=B004X6ABTM'
                ),
                3 => array(
                    'Description' => 'Add To Wishlist',
                    'URL' => 'https://www.amazon.com/gp/registry/wishlist/add-item.html?asin.0=B004X6ABTM&SubscriptionId=AKIAIZ3S74HU3GOABUYQ&tag=food-processr-20&linkCode=xm2&camp=2025&creative=386001&creativeASIN=B004X6ABTM'
                ),
                4 => array(
                    'Description' => 'Tell A Friend',
                    'URL' => 'https://www.amazon.com/gp/pdp/taf/B004X6ABTM?SubscriptionId=AKIAIZ3S74HU3GOABUYQ&tag=food-processr-20&linkCode=xm2&camp=2025&creative=386001&creativeASIN=B004X6ABTM'
                ),
                5 => array(
                    'Description' => 'All Customer Reviews',
                    'URL' => 'https://www.amazon.com/review/product/B004X6ABTM?SubscriptionId=AKIAIZ3S74HU3GOABUYQ&tag=food-processr-20&linkCode=xm2&camp=2025&creative=386001&creativeASIN=B004X6ABTM'
                ),
                6 => array(
                    'Description' => 'All Offers',
                    'URL' => 'https://www.amazon.com/gp/offer-listing/B004X6ABTM?SubscriptionId=AKIAIZ3S74HU3GOABUYQ&tag=food-processr-20&linkCode=xm2&camp=2025&creative=386001&creativeASIN=B004X6ABTM'
                )
            ),
            'imageSet' => array(
                0 => array(
                    'attributes' => array(
                        'Category' => 'variant'
                    ),
                    'SwatchImage' => 'https://images-na.ssl-images-amazon.com/images/I/51pHVksdeGL._SL30_.jpg',
                    'SmallImage' => 'https://images-na.ssl-images-amazon.com/images/I/51pHVksdeGL._SL75_.jpg',
                    'ThumbnailImage' => 'https://images-na.ssl-images-amazon.com/images/I/51pHVksdeGL._SL75_.jpg',
                    'TinyImage' => 'https://images-na.ssl-images-amazon.com/images/I/51pHVksdeGL._SL110_.jpg',
                    'MediumImage' => 'https://images-na.ssl-images-amazon.com/images/I/51pHVksdeGL._SL160_.jpg',
                    'LargeImage' => 'https://images-na.ssl-images-amazon.com/images/I/51pHVksdeGL.jpg'
                ),
                1 => array(
                    'attributes' => array(
                        'Category' => 'variant'
                    ),
                    'SwatchImage' => 'https://images-na.ssl-images-amazon.com/images/I/51wDO-KqVCL._SL30_.jpg',
                    'SmallImage' => 'https://images-na.ssl-images-amazon.com/images/I/51wDO-KqVCL._SL75_.jpg',
                    'ThumbnailImage' => 'https://images-na.ssl-images-amazon.com/images/I/51wDO-KqVCL._SL75_.jpg',
                    'TinyImage' => 'https://images-na.ssl-images-amazon.com/images/I/51wDO-KqVCL._SL110_.jpg',
                    'MediumImage' => 'https://images-na.ssl-images-amazon.com/images/I/51wDO-KqVCL._SL160_.jpg',
                    'LargeImage' => 'https://images-na.ssl-images-amazon.com/images/I/51wDO-KqVCL.jpg'
                ),
                2 => array(
                    'attributes' => array(
                        'Category' => 'variant'
                    ),
                    'SwatchImage' => 'https://images-na.ssl-images-amazon.com/images/I/51H8t3YLwsL._SL30_.jpg',
                    'SmallImage' => 'https://images-na.ssl-images-amazon.com/images/I/51H8t3YLwsL._SL75_.jpg',
                    'ThumbnailImage' => 'https://images-na.ssl-images-amazon.com/images/I/51H8t3YLwsL._SL75_.jpg',
                    'TinyImage' => 'https://images-na.ssl-images-amazon.com/images/I/51H8t3YLwsL._SL110_.jpg',
                    'MediumImage' => 'https://images-na.ssl-images-amazon.com/images/I/51H8t3YLwsL._SL160_.jpg',
                    'LargeImage' => 'https://images-na.ssl-images-amazon.com/images/I/51H8t3YLwsL.jpg'
                ),
                3 => array(
                    'attributes' => array(
                        'Category' => 'variant'
                    ),
                    'SwatchImage' => 'https://images-na.ssl-images-amazon.com/images/I/51T%2BhFRJTJL._SL30_.jpg',
                    'SmallImage' => 'https://images-na.ssl-images-amazon.com/images/I/51T%2BhFRJTJL._SL75_.jpg',
                    'ThumbnailImage' => 'https://images-na.ssl-images-amazon.com/images/I/51T%2BhFRJTJL._SL75_.jpg',
                    'TinyImage' => 'https://images-na.ssl-images-amazon.com/images/I/51T%2BhFRJTJL._SL110_.jpg',
                    'MediumImage' => 'https://images-na.ssl-images-amazon.com/images/I/51T%2BhFRJTJL._SL160_.jpg',
                    'LargeImage' => 'https://images-na.ssl-images-amazon.com/images/I/51T%2BhFRJTJL.jpg'
                ),
                4 => array(
                    'attributes' => array(
                        'Category' => 'variant'
                    ),
                    'SwatchImage' => 'https://images-na.ssl-images-amazon.com/images/I/31hpk2kIdDL._SL30_.jpg',
                    'SmallImage' => 'https://images-na.ssl-images-amazon.com/images/I/31hpk2kIdDL._SL75_.jpg',
                    'ThumbnailImage' => 'https://images-na.ssl-images-amazon.com/images/I/31hpk2kIdDL._SL75_.jpg',
                    'TinyImage' => 'https://images-na.ssl-images-amazon.com/images/I/31hpk2kIdDL._SL110_.jpg',
                    'MediumImage' => 'https://images-na.ssl-images-amazon.com/images/I/31hpk2kIdDL._SL160_.jpg',
                    'LargeImage' => 'https://images-na.ssl-images-amazon.com/images/I/31hpk2kIdDL.jpg'
                ),
                5 => array(
                    'attributes' => array(
                        'Category' => 'primary'
                    ),
                    'SwatchImage' => 'https://images-na.ssl-images-amazon.com/images/I/51YT8wAQeUL._SL30_.jpg',
                    'SmallImage' => 'https://images-na.ssl-images-amazon.com/images/I/51YT8wAQeUL._SL75_.jpg',
                    'ThumbnailImage' => 'https://images-na.ssl-images-amazon.com/images/I/51YT8wAQeUL._SL75_.jpg',
                    'TinyImage' => 'https://images-na.ssl-images-amazon.com/images/I/51YT8wAQeUL._SL110_.jpg',
                    'MediumImage' => 'https://images-na.ssl-images-amazon.com/images/I/51YT8wAQeUL._SL160_.jpg',
                    'LargeImage' => 'https://images-na.ssl-images-amazon.com/images/I/51YT8wAQeUL.jpg'
                )
            ),
            'AmountSaved' => '$14.26',
            'PercentageSaved' => '',
            'IsEligibleForSuperSaverShipping' => '1',
            'customerReviews' => array(),
            'editorialReviews' => array(),
            'smallImage' => 'https://images-na.ssl-images-amazon.com/images/I/51YT8wAQeUL._SL75_.jpg',
            'mediumImage' => 'https://images-na.ssl-images-amazon.com/images/I/51YT8wAQeUL._SL160_.jpg',
            'largeImage' => 'https://images-na.ssl-images-amazon.com/images/I/51YT8wAQeUL.jpg',
            'addToCartUrl' => 'http://www.amazon.com/gp/aws/cart/add.html?ASIN.1=B004X6ABTM&Quantity.1=1&AssociateTag=food-processr-20',
            'ASIN' => 'B004X6ABTM',
            'itemAttributes' => array(
                'Actor' => '',
                'Artist' => '',
                'AspectRatio' => 'Unknown',
                'AudienceRating' => '',
                'AudioFormat' => '',
                'Binding' => 'Personal Computers',
                'Brand' => 'MSI',
                'CEROAgeRating' => '',
                'ClothingSize' => '',
                'Color' => '',
                'Creator' => '',
                'Department' => '',
                'Director' => '',
                'EAN' => '8169090853896',
                'EANList' => '',
                'Edition' => '',
                'EISBN' => '',
                'EpisodeSequence' => '',
                'ESRBAgeRating' => '',
                'Feature' => array(
                    0 => 'Chipset: Radeon HD6450 Engine Clock: 625 MHz Video Memory: 1GB DDR3',
                    1 => 'Memory Clock: 1333 MHz Memory Interface: 64-bit Bus: PCI-Express 2.1 x16',
                    2 => 'RAMDAC: 400 MHz Max. Resolution: 2560 x 1600 Connectors: DVI; VGA; HDMI',
                    3 => 'Thermal: Fansink Support Microsoft Windows 7 Support ATI CrossFireX Technology',
                    4 => 'Support AMD Stream Technology Support Blu-ray and HD DVD',
                    5 => 'Support HDCP - High-bandwidth Digital Content Protection.Interface:PCI Express x16 2.1',
                    6 => 'Support Microsoft DirectX 11 and OpenGL 4.1 Low Profile Design'
                ),
                'Format' => '',
                'Genre' => '',
                'HardwarePlatform' => '',
                'HazardousMaterialType' => '',
                'IsAdultProduct' => '',
                'IsAutographed' => '0',
                'ISBN' => '',
                'IsEligibleForTradeIn' => '',
                'IsMemorabilia' => '0',
                'IssuesPerYear' => '',
                'ItemDimensions' => '',
                'ItemPartNumber' => '',
                'Label' => 'MSI COMPUTER',
                'Languages' => '',
                'LegalDisclaimer' => 'BRAND NEW NEVER OPENED IN THE ORIGINAL BOX! Shipping on the same or next business day with checking number!',
                'ManufacturerMaximumAge' => '',
                'ManufacturerMinimumAge' => '',
                'ManufacturerPartsWarrantyDescription' => '',
                'MediaType' => '',
                'Model' => 'R6450-MD1GD3/LP',
                'MPN' => 'R6450-MD1GD3/LP',
                'NumberOfDiscs' => '',
                'NumberOfIssues' => '',
                'NumberOfItems' => '1',
                'NumberOfPages' => '',
                'NumberOfTracks' => '',
                'OperatingSystem' => '',
                'PackageQuantity' => '1',
                'PartNumber' => 'R6450-MD1GD3/LP',
                'Platform' => '',
                'ProductGroup' => 'CE',
                'ProductTypeSubcategory' => '',
                'PublicationDate' => '',
                'Publisher' => 'MSI COMPUTER',
                'RegionCode' => '',
                'ReleaseDate' => '',
                'RunningTime' => '',
                'SeikodoProductCode' => '',
                'Size' => '1GB',
                'SKU' => '',
                'Studio' => 'MSI COMPUTER',
                'SubscriptionLength' => '',
                'TradeInValue' => '',
                'UPC' => '071030051492',
                'UPCList' => '',
                'Warranty' => '',
                'WEEETaxValue' => '',
                'PackageDimensions' => ''
            ),
            'toLowToDisplay' => '',
            'availability' => 'Usually ships in 24 hours',
            'lowestNewPrice' => '44.96',
            'lowestUsedPrice' => '24.96',
            'lowestCollectiblePrice' => '',
            'lowestRefurbishedPrice' => '39.98',
            'totalNew' => '42',
            'totalUsed' => '9',
            'totalCollectible' => '0',
            'totalRefurbished' => '1',
            'date' => '',
            'author' => '',
            'source' => '',
            'domain' => ''
        ),
        'added' => '1',
        'keyword' => 'graphic card',
        'img_file' => '2018/04/msi-ati-radeon-hd6450-1-gb-ddr3-vgadvihdmi-low-profile-pci-express-video.jpg'
    )
);


$keys = array_keys( $products);
print_r($products[$keys[0]]['price']);
print_r($products[$keys[0]]['currency']);