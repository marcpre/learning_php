<?php
$message = 'hello';

// No "use"
$example = function () {
    var_dump($message);
};
print_r($example());


// Inherit $message
$example = function () use ($message) {
    var_dump($message);
};
print_r($example());
