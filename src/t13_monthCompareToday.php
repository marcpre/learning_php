<?php
    
$date = '01/11/2012';
$date = str_replace('/', '-', $date);
$releaseDate =  date('Y-m-d H:i:s', strtotime($date)); // November, 1st 2018
$today = date("Y-m-d H:i:s");
    
echo "releaseDate: " . $releaseDate . "\n";
echo "today: " . $today . "\n \n";
if ( $today <= $releaseDate ) {
    echo "Date is in the future: " . $releaseDate;
} else {
    echo "Date is in the past: " . $releaseDate;
}
    
echo "\n";