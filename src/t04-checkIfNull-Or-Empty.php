<?php

$a = null;
$b = "string";

print_r(is_null($a) . "\n");
print_r(($a === NULL ? "Var is Null." : "Var is NOT Null."));

print_r(isset($a) . "\n"); // Determine if a variable is set and is not NULL
print_r(isset($b) . "\n"); 


print_r(empty($b) . "\n"); // Determine whether a variable is empty
print_r(empty($a) . "\n");
